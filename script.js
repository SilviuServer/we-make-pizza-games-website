
	/* http://smuwn.xyz */

	var $ = function(_){return"undefined"===typeof _?document:document.querySelectorAll(_)};

	function shuffle(a) {
		var j, x, i;
		for (i = a.length - 1; i > 0; i--) {
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
		}
		return a;
	}

	var playlist = [
		{name: "COVALENT - Nutcracker's Xtasy (Nutcracker's Tale Remix)", id: "b4DJT-latn4"},
		{name: "COVALENT - Mr. Twister", id: "qudAC1naOxw"},
		{name: "COVALENT - It's Showtime", id: "_mWZ3Wndqnw"},
		{name: "COVALENT - Monster Energy", id: "Nn2jIXn3dsA"},
		{name: "COVALENT - OverHyped", id: "zW-QkPfxbOU"},
		{name: "COVALENT - Robodrop", id: "yYKpoYGphsY"},
		{name: "COVALENT - 270617", id: "kSU28eN8wSY"},
        { name: "COVALENT - Sine", id: "6bRCfJ94dMg" },
        { name: "COVALENT - Wake Up, Romania!", id: "fslxmYR98L8" },
        { name: "COVALENT - hold my hand at dusk", id: "jv13tKfvXXM" }
	],
		player,
		currentsong = 0,
		musicbutton = $(".header .music>span")[0],
		musictitle = $(".header .music>p")[0],
		musicplay = 0,
		linkinterval;

	function changemusic(param){
		var id = {};
		id.suggestedQuality = "small";
		if(param === undefined){
			id.videoId = playlist[currentsong].id;
			musictitle.innerHTML = "<a target='_blank' href='https://youtube.com/watch?v=" + id.videoId + "'>" + playlist[currentsong].name + "</a>";
		}
		else{
			if(currentsong == playlist.length) currentsong = 0;
			else currentsong++;
			id.videoId = playlist[currentsong].id;
			musictitle.innerHTML = "<a target='_blank' href='https://youtube.com/watch?v=" + id.videoId + "'>" + playlist[currentsong].name + "</a>";
		}
		player.loadVideoById(id);
		player.setVolume(50);
		if(musicplay) player.playVideo();
		else player.pauseVideo();
	}
	function togglemusic(){
		if(musicplay){
			player.pauseVideo();
			musicplay = 0;
			musicbutton.innerHTML = "<svg><use xlink:href='#mplay'></use></svg>";
		}
		else{
			player.playVideo();
			musicplay = 1;
			musicbutton.innerHTML = "<svg><use xlink:href='#mpause'></use></svg>";
		}
	}
	function pausemusic(){
		if(musicplay) togglemusic();
	}
	function onplayerready(){
		player.setVolume(50);
		changemusic();
		musicbutton.innerHTML = "<svg><use xlink:href='#mplay'></use></svg>";
		musicbutton.addEventListener("click", togglemusic);
		musictitle.addEventListener("click", pausemusic);
	}
	function onplayerstatechange(param){
		if(param.data === 0) changemusic({});
		else if(param.data === 1) linkinterval = setInterval(function(){
			var musiclink = musictitle.querySelector("a"),
				linkname = musiclink.getAttribute("href").replace(/\&t=.*/g, "");
			musiclink.setAttribute("href", linkname + "&t=" + Math.floor(player.getCurrentTime()) + "s");
		}, 1000);
		else if(param.data === 2) clearInterval(linkinterval);
	}
	function onYouTubePlayerAPIReady(){
		player = new YT.Player("player", {events: {onReady: onplayerready, onStateChange: onplayerstatechange}, playerVars: {autohide: 1, modestbranding: 1, rel: 0, showinfo: 0, controls: 0, enablejsapi: 1, iv_load_policy: 3}});
	}
	function music(){
		shuffle(playlist);
		var script = document.createElement("script");
        script.src = "https://www.youtube.com/player_api";
        document.body.appendChild(script);
	}

(function(){

    document.addEventListener("DOMContentLoaded", dom);

    var state = "", ytplay = false, img = false;

    function youtube(el){
        if(ytplay) return;
		pausemusic();

        var url;

        if(typeof el === "string") url = el;
        else if(typeof el === "object") url = el.currentTarget.getAttribute("data-yt");
        else throw new Error("el parameter incorrect");
        if(!url) throw new Error("el parameter incorrect");

        var c = document.createElement("div"),
            o = document.createElement("div"),
            cl = document.createElement("div");
        cl.innerHTML = '<svg><use xlink:href="#close"></use></svg>';
        c.className = "yt-container";
        o.className = "yt-overlay";
        c.appendChild(cl);
        c.innerHTML += '<iframe src="https://www.youtube.com/embed/' + url + '?rel=0&amp;showinfo=0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        document.body.appendChild(c);
        document.body.appendChild(o);
        ytplay = true;

        function close_yt(){
            if(~document.body.className.indexOf(" yt-open")) document.body.className = document.body.className.replace(" yt-open", "");
            $(".yt-container>div")[0].removeEventListener("click", close_yt);
            setTimeout(function(){
                document.body.removeChild(c);
                document.body.removeChild(o);
                ytplay = false;
            }, 400)
        }

        setTimeout(function(){
            if(!~document.body.className.indexOf(" yt-open")) document.body.className += " yt-open";
            $(".yt-container>div")[0].addEventListener("click", close_yt);
        }, 100);
    }

    function zoom(el){
        var target = el.currentTarget;

        if(img || target.tagName !== "IMG") return;

        var parent = document.createElement("div"),
            url = target.src;
        parent.className = "img-container";
        parent.innerHTML = "<div class=\"load\"><svg><use xlink:href=\"#load\"></use></svg></div>";

        document.body.appendChild(parent);
        img = true;

        function close(){
            if(~parent.className.indexOf("show")) parent.className = parent.className.replace(" show", "");
            setTimeout(function(){
                parent.removeEventListener("click", close);
                parent.outerHTML = "";
                parent = null;
                img = false;
            }, 400);
        }
        function onload(){
            parent.querySelector("div.load").outerHTML = "<img src=\"" + url + "\"/>";
            setTimeout(function(){
                if(!~parent.className.indexOf("load")) parent.className += " load";
            }, 100);
        }
        function onerror(){
            parent.querySelector("div.load").outerHTML = "<p>Something wrong happened. Please try again</p>";
        }

        setTimeout(function(){

            if(!~parent.className.indexOf("show")) parent.className += " show";
            parent.addEventListener("click", close);

            var l = new Image();
            l.addEventListener("load", onload);
            l.addEventListener("error", onerror);
            l.src = url;

        }, 100);
    }

    function dom(){
        var preloader = $("body>.loader .forth path")[0];
        var t1 = setTimeout(function(){
            preloader.style.strokeDashoffset = "350";
        }, 900);
        var t2 = setTimeout(function(){
            preloader.style.strokeDashoffset = "250";
        }, 1900);
        var t3 = setTimeout(function(){
            preloader.style.strokeDashoffset = "100";
        }, 2800);

        var home = $(".header img")[0],
            contact = $(".header .contact")[0],
            games = $(".header .games")[0],
            people = $(".header .people")[0];

		var currentvideo = null;

		function checkvideoclass(e){
			var div = $("body>.background>." + e),
				defaults = $("body>.background>div"),
				vid;

			for(var i = 0; i < defaults.length; i++){
				if(div[0] != defaults[i]) defaults[i].classList.remove("show");
			}

			if(currentvideo){
				currentvideo.pause();
				currentvideo = null;
			}

			if(div.length > 0){
				div[0].classList.add("show");
				vid = div[0].querySelector("video");
				currentvideo = vid;
				vid.play();
			}
		}

        function contactf(){
            function close(){
                contact.addEventListener("click", contactf);
                $("body>.main")[0].removeEventListener("click", close);
                if(~document.body.className.indexOf("contact")) document.body.className = document.body.className.replace(" contact", "");
            }
            if(!~document.body.className.indexOf("contact")) document.body.className += " contact";
            setTimeout(function(){
                contact.removeEventListener("click", contactf);
                $("body>.main")[0].addEventListener("click", close);
            }, 300);
        }

        contact.addEventListener("click", contactf);

        var q /* cant find a proper name sry lmao */ = $(".main>.wrapper>.games>div"),
            games_w = $(".main>.wrapper>.games")[0];

        function add_ev(){
            home.addEventListener("click", change_state);
            games.addEventListener("click", change_state);
            people.addEventListener("click", change_state);

            for(var i = 0; i < q.length; i++) if(!~q[i].className.indexOf("coming")) q[i].addEventListener("click", change_state);
        }
        function remove_ev(){
            home.removeEventListener("click", change_state);
            games.removeEventListener("click", change_state);
            people.removeEventListener("click", change_state);

            for(var i = 0; i < q.length; i++) if(!~q[i].className.indexOf("coming")) q[i].removeEventListener("click", change_state);
        }

        function change(current, next, time){
            var time = time ? time : 400,
                ytframe = document.querySelector(".main>.wrapper>.home iframe"),
                temp;

            remove_ev();

            if(~document.body.className.indexOf(" show")) document.body.className = document.body.className.replace(" show", "");
            setTimeout(function(){
                if(~document.body.className.indexOf(" " + current)) document.body.className = document.body.className.replace(" " + current, "");
                if(!~document.body.className.indexOf(" " + next)) document.body.className += " " + next;
                setTimeout(function(){
                    if(!~document.body.className.indexOf(" show")) document.body.className += " show";
					checkvideoclass(next);
                    add_ev();
                }, 100);
                temp = ytframe.src;
                ytframe.src = "";
                ytframe.src = temp;
            }, time);

            state = next;
        }

        function change_state(a){
            var target = a.currentTarget,
                cstat = state ? state : "home",
                next;

            if(target == home) next = "home";
            else if(target == games) next = "games";
            else if(target == people) next = "people";
            else if(target.parentNode == games_w && target.tagName == "DIV"){
                if(~target.className.indexOf("hop")) next = "game-hop";
                else if(~target.className.indexOf("castle")) next = "game-castle";
                else if(~target.className.indexOf("fancy2")) next = "game-fancy2";
                else if(~target.className.indexOf("fancy")) next = "game-fancy";
				else if(~target.className.indexOf("retro")) next = "game-retro";
            }


            if(state == next || (next == "home" && !state)) return;

            if(next == "home" || next == "games" || next == "people" || ~next.indexOf("game-")) change(cstat, next);
        }

        function em(){
            const p = $("body>.contact>a")[0],
                  o = ["70", "69", "7A", "7A", "61"],
                  g = ["2E", "67", "61", "6D", "65", "73", "30"],
                  f = ["33", "31", "32", "40", "67", "6D", "61", "69", "6C", "2E", "63", "6F", "6D"],
                  z = "&#x",
                  l = ";";
            p.href = "mailto:";
            for(var h = 0; h < o.length; h++) p.innerHTML += z + o[h] + l;
            for(var u = 0; u < g.length; u++) p.innerHTML += z + g[u] + l;
            for(var q = 0; q < f.length; q++) p.innerHTML += z + f[q] + l;
            p.href += p.innerHTML;
        }

        function load(){

            clearTimeout(t1); clearTimeout(t2); clearTimeout(t3);
            setTimeout(function(){
                preloader.setAttribute("style", "stroke-dashoffset: 0;");
                document.body.className += " home";
                setTimeout(function(){
                    document.body.className += " loaded show";
                }, 200);
            }, 900);

            add_ev();

            var yt_p = $("[data-yt]");
            for(var i = 0; i < yt_p.length; i++) yt_p[i].addEventListener("click", youtube);

            var d = $("img[data-src]");
            for(var i = 0; i < d.length; i++) d[i].src = d[i].getAttribute("data-src");

            em();
			music();
        }

        window.addEventListener("load", load);

        var zoomimg = $("img._zoom");
        for(var i = 0; i < zoomimg.length; i++) zoomimg[i].addEventListener("click", zoom);

    }
})();